# -*- coding: utf-8 -*-

## \package command_line

# MIT licensing
# See: docs/LICENSE.txt


import sys

from startup.tests import available_tests
from startup.tests import test_list


## Solo args
#
#  -h or --help
#	Display usage information in the command line.
#  -v or --version
#	Display Debreate version in the command line & exit
solo_args = (
	(u'h', u'help'),
	(u'v', u'version'),
)

## Value args
#
#  -l or --log-level
#	Sets logging output level. Values can be 'quiet', 'info', 'warn', 'error', or debug,
#	or equivalent numeric values of 0-4. Default is 'error' (3).
#  -i or --log-interval
#	Set the refresh interval, in seconds, for updating the log window.
value_args = (
	(u'l', u'log-level'),
	(u'i', u'log-interval'),
)

cmds = (
	u'clean',
	u'compile',
	u'legacy',
	u'test',
)

parsed_args_s = []
parsed_args_v = {}
parsed_commands = []
parsed_path = None


def ArgOK(arg, group):#arg是group中的元素中的一个参数
	for s, l in group:
		if arg in (s, l,):
			return True

	return False


def ArgIsDefined(arg, a_type):
	for group in (solo_args, value_args):
		for SET in group:
			for A in SET:
				if arg == A:
					return True

	return False

#返回类型包括了command，long，long-value（有2个-，以有无=区分是否是值）short，short-value（有1个-号），path
def GetArgType(arg):
	dashes = 0
	for C in arg: #参数里有dashes个-字符
		if C != u'-':
			break

		dashes += 1

	if dashes:
		if dashes == 2 and len(arg.split(u'=')[0]) > 2:#arg里面有2个-号，第一个=号前面的字串长度大于2
			if not arg.count(u'='): #没有等号
				return u'long'

			if arg.count(u'=') == 1:#有一个等号
				return u'long-value'

		elif dashes == 1 and len(arg.split(u'=')[0]) == 2:#arg里面有1个-号，第一个=号前面的字串长度等于2
			if not arg.count(u'='):#没有等号
				return u'short'

			if arg.count(u'=') == 1:#有一个等号
				return u'short-value'

		return None

	if arg in cmds:#命令有clean，compile，legacy，test
		return u'command'

	# Any other arguments should be a filename path
	return u'path'


def ParseArguments(arg_list):
	global parsed_path, parsed_commands, parsed_args_s, parsed_args_v

	if u'test' in arg_list:
		testcmd_index = arg_list.index(u'test')
		tests = arg_list[testcmd_index+1:]

		if not tests:
			print(u'ERROR: Must supply at least one test')
			sys.exit(1)

		for TEST in tests:
			if TEST not in available_tests:
				print(u'ERROR: Unrecognized test: {}'.format(TEST))
				sys.exit(1)

			test_list.append(TEST)

			# Remove tests from arguments移除tests选项命令
			arg_list.pop(testcmd_index + 1)

		# Remove test command from arguments移除test命令
		arg_list.pop(testcmd_index)

	argc = len(arg_list) #这个时候的arg_list应该只有后跟的参数，test的东西都没了

	for AINDEX in range(argc):#对arg_list做个遍历
		if AINDEX >= argc:
			break

		A = arg_list[AINDEX] #A是arg_list中的值
		arg_type = GetArgType(A)

		if arg_type == None:
			print(u'ERROR: Malformed argument: {}'.format(A))
			sys.exit(1)

		if arg_type == u'command':
			parsed_commands.append(A)
			continue

		if arg_type == u'path':
			if parsed_path != None:
				print(u'ERROR: Extra input file detected: {}'.format(A))#没有识别的输入，就是说参数里只能有1个路径
				# FIXME: Use errno here
				sys.exit(1)

			parsed_path = A
			continue

		clip = 0
		for C in A:
			if C != u'-':
				break

			clip += 1

		if arg_type in (u'long', u'short'):
			parsed_args_s.append(A[clip:])#添加去掉-号后的参数
			continue

		# Anything else should be a value type
		key, value = A.split(u'=')

		# FIXME: Value args can be declared multiple times

		if not value.strip():#参数项没有值
			print(u'ERROR: Value argument with empty value0: {}'.format(key))
			# FIXME: Use errno here
			sys.exit(1)

		key = key[clip:]

		# Use long form使用全称参数
		for S, L in value_args:#一个图，里面有两个，简写和全称参数含义，-l,-i,loglevel,和logintevenal
			if key == S:
				key = L
				break

		# Allow using 'warning' as 'alias' for 'log-level'
		if key == u'log-level' and value == u'warning':#把warn的别名换成正名
			value = u'warn'

		parsed_args_v[key] = value#就它是图，其他三个参数是列表

	for A in parsed_args_s:#这一次循环是为了查找输入的命令是不是-h或者-v系列，如果是的话变成全称，不是的话***
		if not ArgOK(A, solo_args):#这次输入的不是图中命令，solo_args是另一个图，里面两个-h和-v参数和他们的全称
			for S, L in value_args:#循环-l和-i的图
				if A in (S, L,):#判断是不是这次输入的命令是-l和-i的参数，但是忘了输入等号和值
					print(u'ERROR: Value argument with empty value1: {}'.format(A))
					# FIXME: Use errno here:
					sys.exit(1)

			print(u'ERROR: Unknown argument1: {}'.format(A))
			# FIXME: Use errno here
			sys.exit(1)

		# Use long form使用完整参数模式
		arg_index = parsed_args_s.index(A)
		for S, L in solo_args:
			if A == S:
				parsed_args_s[arg_index] = L

	for A in parsed_args_v:#这一次循环是为了查找是不是-l或者-i参数，加上上次判断可判断是不是正确参数
		if not ArgOK(A, value_args):
			print(u'ERROR: Unknown argument2: {}'.format(A))
			# FIXME: Use errno here
			sys.exit(1)

	for S, L in solo_args:#查找是否同时使用了别称参数和全称参数
		s_count = parsed_args_s.count(S)
		l_count = parsed_args_s.count(L)

		if s_count + l_count > 1:
			print(u'ERROR: Duplicate arguments: -{}|--{}'.format(S, L))
			# FIXME: Use errno here
			sys.exit(1)


## Checks if an argument was used
def FoundArg(arg):
	for group in (parsed_args_s, parsed_args_v):
		for A in group:
			if A == arg:
				return True

	return False


## Checks if a command was used
def FoundCmd(cmd):
	return cmd in parsed_commands


def GetParsedPath():
	return parsed_path
