# -*- coding: utf-8 -*-

## \package wiz.pginit
#
#  The initial wizard page

# MIT licensing
# See: docs/LICENSE.txt


import wx

from dbr.language	import GT
from globals.ident	import pgid
from ui.hyperlink	import Hyperlink
from ui.layout		import BoxSizer
from wiz.wizard		import WizardPage


## The initial wizard page
class Page(WizardPage):
	def __init__(self, parent):
		WizardPage.__init__(self, parent, pgid.GREETING)

		m1 = GT(u'欢迎使用DebreateForUos!')
		m2 = GT(u'DebreateForUos是用来给安装在UOS/deepin系统上的软件打包的工具,  它基于debreate做了汉化和部分修改内容。 点击右上角的箭头或者 点击页面菜单在整个程序中导航. 要获得其他关于UOS软件包的信息，请使用菜单项 帮助->链接并选择你感兴趣的内容.')
		m3 = GT(u'点击下方链接查看UOS打包标准或者DebreateForUos图文版教程.')
		str_info = u'{}\n\n{}\n\n{}'.format(m1, m2, m3)

		# --- Information to be displayed about each mode
		txt_info = wx.StaticText(self, label=str_info)
		# Keep characters within the width of the window
		txt_info.Wrap(600)

		lnk_video = Hyperlink(self, wx.ID_ANY, GT(u'Uos商店打包标准'),
				u'https://doc.chinauos.com/content/M7kCi3QB_uwzIp6HyF5J')
		lnk_video.SetToolTipString(lnk_video.url)

		lnk_image = Hyperlink(self, wx.ID_ANY, GT(u'使用DebreateForUos构建软件包的图文教程'),
				u'https://bbs.chinauos.com/post/10049')
		lnk_image.SetToolTipString(lnk_image.url)

		# *** Layout *** #

		lyt_info = wx.GridSizer()
		lyt_info.Add(txt_info, 1, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL)

		lyt_main = BoxSizer(wx.VERTICAL)
		lyt_main.Add(lyt_info, 4, wx.ALIGN_CENTER|wx.ALL, 10)
		lyt_main.Add(lnk_video, 2, wx.ALIGN_CENTER)
		lyt_main.Add(lnk_image, 2, wx.ALIGN_CENTER)

		self.SetAutoLayout(True)
		self.SetSizer(lyt_main)
		self.Layout()
