# -*- coding: utf-8 -*-

## \package wizbin.control

# MIT licensing
# See: docs/LICENSE.txt


import os, wx
from wx.combo		   import OwnerDrawnComboBox

from dbr.language		import GT
from dbr.log			import Logger
from fileio.fileio		import ReadFile
from fileio.fileio		import WriteFile
from globals.errorcodes	import dbrerrno
from globals.ident		import btnid
from globals.ident		import inputid
from globals.ident		import pgid
from globals.ident		import chkid
from globals.strings	import TextIsEmpty
from globals.tooltips	import SetPageToolTips
from input.select		import ChoiceESS
from input.select		import ComboBoxESS
from input.text			import TextAreaESS
from input.text			import TextAreaPanelESS
from input.toggle		import CheckBoxESS, CheckBox
from ui.button			import CreateButton
from ui.dialog			import GetFileOpenDialog
from ui.dialog			import GetFileSaveDialog
from ui.dialog			import ShowDialog
from ui.dialog			import ShowErrorDialog
from ui.layout			import BoxSizer
from ui.panel			import BorderedPanel
from ui.style			import layout as lyt
from ui.textpreview		import TextPreview
from wiz.helper			import FieldEnabled
from wiz.helper			import GetField
from wiz.helper			import ErrorTuple
from wiz.helper			import GetFieldValue
from wiz.helper			import GetMainWindow
from wiz.helper			import GetPage
from wiz.wizard			import WizardPage


## This panel displays the field input of the info file
class Page(WizardPage):
	## Constructor
	#
	#  \param parent
	#	Parent <b><i>wx.Window</i></b> instance
	def __init__(self, parent):
		WizardPage.__init__(self, parent, pgid.INFO)

		pnl_bg = wx.Panel(self)

		# Buttons to open, save, & preview info file
		btn_open = CreateButton(pnl_bg, btnid.BROWSE, GT(u'Browse'), u'browse', name=u'btn browse')
		btn_save = CreateButton(pnl_bg, btnid.SAVE, GT(u'Save'), u'save', name=u'btn save')
		btn_preview = CreateButton(pnl_bg, btnid.PREVIEW, GT(u'Preview'), u'preview', name=u'btn prev')

		# *** Required fields *** #

		pnl_require = BorderedPanel(pnl_bg)

		txt_appid = wx.StaticText(pnl_require, label=GT(u'应用标识'), name=u'appid')
		txt_appid.req = True
		self.ti_appid = TextAreaESS(pnl_require, inputid.PACKAGE, name=txt_appid.Name)
		self.ti_appid.req = True
		
		txt_name = wx.StaticText(pnl_require, label=GT(u'应用名称'), name=u'name')
		txt_name.req = True
		self.ti_name = TextAreaESS(pnl_require, inputid.NAME, name=txt_name.Name)
		self.ti_name.req = True

		txt_version = wx.StaticText(pnl_require, label=GT(u'版本'), name=u'version')
		txt_version.req = True
		self.ti_version = TextAreaESS(pnl_require, inputid.VERSION, name=txt_version.Name)
		self.ti_version.req = True

		txt_arch = wx.StaticText(pnl_require, label=GT(u'架构'), name=u'architecture')
		self.chk_checkAmd = CheckBoxESS(pnl_require,label=GT(u'amd64'), name=u'checkamd64')
		self.chk_checkAmd.Default = True
		self.chk_checkAmd.SetValue(True)
		self.chk_checkArm = CheckBoxESS(pnl_require,label=GT(u'arm64'), name=u'checkarm64')
		#self.chk_checkArm.Default = False
		self.chk_checkMips = CheckBoxESS(pnl_require,label=GT(u'mips64el'), name=u'checkmips64')
		#self.chk_checkMips.Default = False
		self.chk_checkSw = CheckBoxESS(pnl_require,label=GT(u'sw_64'), name=u'checksw64')
		#self.chk_checkSw.Default = False
		btn_import = CreateButton(pnl_require, btnid.IMPORT, GT(u'Import'), u'import', name=u'btn import')
		txt_import = wx.StaticText(pnl_require, label=GT(u'从control页面导入信息'))

		# *** 权限 fields *** #

		pnl_permissions = BorderedPanel(pnl_bg)

		txt_autostart = wx.StaticText(pnl_permissions,label=GT(u'是否自动启动'),name =u'autostart')
		self.chk_autostart = CheckBoxESS(pnl_permissions,label=u' ', name=u'autostart')
		self.chk_autostart.Default=False
		self.chk_autostart.SetValue(False)
		
		txt_notification = wx.StaticText(pnl_permissions,label=GT(u'是否使用通知'),name =u'notification')
		self.chk_notification=CheckBoxESS(pnl_permissions ,label=u' ', name=u'notification')
			
		txt_trayicon= wx.StaticText(pnl_permissions,label=GT(u'是否显示托盘图标'),name =u'trayicon')
		self.chk_trayicon=CheckBoxESS(pnl_permissions,label=u' ', name=u'trayicon')
		
		txt_clipboard = wx.StaticText(pnl_permissions,label=GT(u'是否允许使用剪切板'),name =u'clipboard')
		self.chk_clipboard=CheckBoxESS(pnl_permissions,label=u' ', name=u'clipboard')
		
		txt_account = wx.StaticText(pnl_permissions,label=GT(u'是否读取用户信息'),name =u'account')
		self.chk_account=CheckBoxESS(pnl_permissions,label=u' ', name=u'account')
		
		txt_bluetooth = wx.StaticText(pnl_permissions,label=GT(u'是否允许使用蓝牙'),name =u'bluetooth')
		self.chk_bluetooth=CheckBoxESS(pnl_permissions, label=u' ', name=u'bluetooth')
		
		txt_camera = wx.StaticText(pnl_permissions,label=GT(u'是否允许使用视频设备'),name =u'camera')
		self.chk_camera=CheckBoxESS(pnl_permissions,label=u' ', name=u'camera')
		
		txt_audio_record = wx.StaticText(pnl_permissions,label=GT(u'是否允许录音'),name =u'audio_record')
		self.chk_audio_record=CheckBoxESS(pnl_permissions,label=u' ', name=u'audio_record')
		
		txt_installed_apps = wx.StaticText(pnl_permissions,label=GT(u'是否允许读取已安装软件列表'),name =u'installed_apps')
		self.chk_installed_apps=CheckBoxESS(pnl_permissions,label=u' ', name=u'installed_apps')
		
		

		# *** 插件 fields *** #

		pnl_plugins = BorderedPanel(pnl_bg)

		txt_support_plugins = wx.StaticText(pnl_plugins, label=GT(u'支持的插件类型'), name=u'support_plugins')
		self.ti_support_plugins = TextAreaESS(pnl_plugins, name=txt_support_plugins.Name)

		txt_plugins = wx.StaticText(pnl_plugins, label=GT(u'实现的插件类型'), name=u'plugins')
		self.ti_plugins = TextAreaESS(pnl_plugins, name=txt_plugins.Name)


		
		# *** 输入组织 *** #

		self.grp_input = (
			self.ti_appid,
			self.ti_name,
			self.ti_version,  # Maintainer must be listed before email
			self.ti_support_plugins,
			self.ti_plugins,			
			)
		self.arh_check = (
			self.chk_checkAmd,
			self.chk_checkArm,
			self.chk_checkMips,
			self.chk_checkSw
			)
		self.per_check = (
			self.chk_autostart,
			self.chk_notification,
			self.chk_trayicon,
			self.chk_clipboard,
			self.chk_account,
			self.chk_bluetooth,
			self.chk_camera,
			self.chk_audio_record,
			self.chk_installed_apps
			)

		SetPageToolTips(self)

		# *** Event Handling *** #

		btn_open.Bind(wx.EVT_BUTTON, self.OnBrowse)
		btn_save.Bind(wx.EVT_BUTTON, self.OnSave)
		btn_preview.Bind(wx.EVT_BUTTON, self.OnPreviewInfo)
		btn_import.Bind(wx.EVT_BUTTON, self.OnImportFromInfo)

		# *** Layout *** #

		LEFT_BOTTOM = lyt.ALGN_LB
		RIGHT_CENTER = wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT

		# Buttons
		lyt_buttons = BoxSizer(wx.HORIZONTAL)
		lyt_buttons.Add(btn_open, 0)
		lyt_buttons.Add(btn_save, 0)
		lyt_buttons.Add(btn_preview, 0)

		# Required fields
		lyt_require = wx.FlexGridSizer(3, 4, 5, 5) #(行数，列数，垂直方向行间距，水平方向列间距)
		lyt_require.AddGrowableCol(1)#设置为可增长列
		lyt_require.AddGrowableCol(3)
		
		lyt_arch = BoxSizer(wx.HORIZONTAL)
		lyt_arch.Add(self.chk_checkAmd,0)
		lyt_arch.Add(self.chk_checkArm, 0)
		lyt_arch.Add(self.chk_checkMips,0)
		lyt_arch.Add(self.chk_checkSw,0)

		lyt_require.AddMany((
			(txt_appid, 0, RIGHT_CENTER|lyt.PAD_LT, 5), #（item,proportion缩放比，flag,border）
			(self.ti_appid, 0, wx.EXPAND|wx.TOP, 5),
			(txt_name, 0, RIGHT_CENTER|wx.TOP, 5),
			(self.ti_name, 0, wx.EXPAND|wx.TOP|wx.RIGHT, 5),
			(txt_version, 0, RIGHT_CENTER|wx.LEFT, 5),
			(self.ti_version, 0, wx.EXPAND|wx.ALL,5),		
			(txt_arch, 0, RIGHT_CENTER, 5),
			(lyt_arch, 0, wx.EXPAND|wx.ALL, 5),
			(btn_import, 0, wx.EXPAND|wx.RIGHT, 5),
			(txt_import, 0, wx.EXPAND|wx.RIGHT, 5),
			#(self.chk_checkSw, 0, wx.EXPAND|wx.RIGHT, 5),
			))

		pnl_require.SetSizer(lyt_require)
		pnl_require.SetAutoLayout(True)
		pnl_require.Layout()

		# 权限 fields
		lyt_permissions = wx.FlexGridSizer(5 ,2, 5,5)#子部件间的垂直和水平距离
		lyt_permissions.AddGrowableCol(0) #设置可变长度
		lyt_permissions.AddGrowableCol(1)
		lyt_permissions.AddGrowableRow(0)
		lyt_permissions.AddGrowableRow(3)
		lyt_permissions.AddGrowableRow(1)
		lyt_permissions.AddGrowableRow(2)
		lyt_permissions.AddGrowableRow(4)
		
		lyt_autostart = BoxSizer(wx.HORIZONTAL)
		lyt_autostart.Add(txt_autostart, 0)
		lyt_autostart.Add(self.chk_autostart)
		lyt_permissions.Add(lyt_autostart, 0, wx.EXPAND|wx.ALL, border=5)
		lyt_notification = BoxSizer(wx.HORIZONTAL)
		lyt_notification.Add(txt_notification)
		lyt_notification.Add(self.chk_notification)
		lyt_permissions.Add(lyt_notification, 0, wx.EXPAND|wx.ALL, 5)
		lyt_trayicon = BoxSizer(wx.HORIZONTAL)
		lyt_trayicon.Add(txt_trayicon, 0)
		lyt_trayicon.Add(self.chk_trayicon)
		lyt_permissions.Add(lyt_trayicon,0,wx.EXPAND|wx.ALL, 5)
		lyt_clipboard = BoxSizer(wx.HORIZONTAL)
		lyt_clipboard.Add(txt_clipboard, 0)
		lyt_clipboard.Add(self.chk_clipboard)
		lyt_permissions.Add(lyt_clipboard,0,wx.EXPAND|wx.ALL, 5)
		lyt_account = BoxSizer(wx.HORIZONTAL)
		lyt_account.Add(txt_account, 0)
		lyt_account.Add(self.chk_account)
		lyt_permissions.Add(lyt_account,0,wx.EXPAND|wx.ALL, 5)
		lyt_bluetooth = BoxSizer(wx.HORIZONTAL)
		lyt_bluetooth.Add(txt_bluetooth, 0)
		lyt_bluetooth.Add(self.chk_bluetooth)
		lyt_permissions.Add(lyt_bluetooth,0,wx.EXPAND|wx.ALL, 5)
		lyt_camera = BoxSizer(wx.HORIZONTAL)
		lyt_camera.Add(txt_camera, 0)
		lyt_camera.Add(self.chk_camera)
		lyt_permissions.Add(lyt_camera,0,wx.EXPAND|wx.ALL, 5)
		lyt_audiorecord = BoxSizer(wx.HORIZONTAL)
		lyt_audiorecord.Add(txt_audio_record, 0)
		lyt_audiorecord.Add(self.chk_audio_record)
		lyt_permissions.Add(lyt_audiorecord,0,wx.EXPAND|wx.ALL, 5)	
		lyt_installedapps = BoxSizer(wx.HORIZONTAL)
		lyt_installedapps.Add(txt_installed_apps, 0)
		lyt_installedapps.Add(self.chk_installed_apps)
		lyt_permissions.Add(lyt_installedapps,0,wx.EXPAND|wx.ALL, 5)	
			

		pnl_permissions.SetSizer(lyt_permissions)
		pnl_permissions.SetAutoLayout(True)
		pnl_permissions.Layout()

		# plugins fields
		lyt_plugins = wx.FlexGridSizer(2,2, 5, 5)

		lyt_plugins.AddGrowableCol(1)
		lyt_plugins.AddMany((
			(txt_support_plugins, 0, wx.EXPAND|wx.ALL, 5),
			(self.ti_support_plugins, 0, wx.EXPAND|wx.ALL,  5),
			(txt_plugins, 0, wx.EXPAND|wx.ALL, 5),
			(self.ti_plugins, 0, wx.EXPAND|wx.ALL, 5),
			))

		pnl_plugins.SetSizer(lyt_plugins)
		pnl_plugins.SetAutoLayout(True)
		pnl_plugins.Layout()

		# Main background panel sizer
		# FIXME: Is background panel (pnl_bg) necessary
		lyt_bg = BoxSizer(wx.VERTICAL)
		lyt_bg.Add(lyt_buttons, 0, wx.ALIGN_RIGHT|wx.BOTTOM, 5)
		lyt_bg.Add(wx.StaticText(pnl_bg, label=GT(u'必填项目')), 0)
		lyt_bg.Add(pnl_require, 0, wx.EXPAND)
		lyt_bg.Add(wx.StaticText(pnl_bg, label=GT(u'应用权限')), 0, wx.TOP, 5)
		lyt_bg.Add(pnl_permissions, 1, wx.EXPAND)
		lyt_bg.Add(wx.StaticText(pnl_bg, label=GT(u'插件选项')), 0, wx.TOP, 5)
		lyt_bg.Add(pnl_plugins, 0, wx.EXPAND)

		pnl_bg.SetAutoLayout(True)
		pnl_bg.SetSizer(lyt_bg)
		pnl_bg.Layout()

		# Page's main sizer
		lyt_main = BoxSizer(wx.VERTICAL)
		lyt_main.AddSpacer(5)
		lyt_main.Add(pnl_bg, 1, wx.EXPAND|lyt.PAD_LR|wx.BOTTOM, 5)

		self.SetAutoLayout(True)
		self.SetSizer(lyt_main)
		self.Layout()


	## Retrieves information for info file export
	#
	#  \return
	#	A <b><i>tuple</i></b> containing the filename & a string representation
	#	of control file formatted for text output
	def Get(self):
		return self.GetInfo()


	## Retrieves field values & formats into plain text for output to file
	#
	#  \return
	#	Control file text
	def GetInfo(self):

		info_list = []
		arch = u''
		permissions = None

		# appid, name, version
		for field in self.grp_input:
			field_name = field.GetName()
			field_value = field.GetValue()

			if FieldEnabled(field) and not TextIsEmpty(field_value):				

				# Strip leading & trailing spaces, tabs, & newlines
				field_value = field_value.strip(u' \t\n')

				if field_name in { u'appid', u'name',u'version'}:
					Logger.Debug(__name__, GT(u'输出 {} 变量').format(field_name))
					info_list.append(u'"{}": "{}"'.format(field_name, field_value))
					continue	

		#arch架构		
		for field in self.arh_check:
			field_name = field.GetLabel()
			field_value = field.GetValue()

			if FieldEnabled(field) and field_value:
				if not arch == u'':
					arch += u','
				arch += u'"{}"'.format(field_name)
		
		Logger.Debug(__name__, GT(u'输出 arch 变量'))
		info_list.append(u'"{}": [{}]'.format(u'arch', arch))
		
		#permissions权限
		permissions = u'{\n'
		for field in self.per_check:
			field_name = field.GetName()
			field_value = field.GetValue()
			
			if FieldEnabled(field):
				if not field_value:
					permissions += u'    "{}": false'.format(field_name)
				else:
					permissions += u'    "{}": true'.format(field_name)
				if not field == self.chk_installed_apps:#不是最后一名，加逗号和回车
					permissions += u',\n'
		permissions += u'\n  }'
		Logger.Debug(__name__, GT(u'输出 permissions 变量'))
		info_list.append(u'"{}": {}'.format(u'permissions', permissions))

		# plugins选项
		for field in self.grp_input:
			field_name = field.GetName()
			field_value = field.GetValue().strip(u'\t ')

			if FieldEnabled(field) and not TextIsEmpty(field_value):

				if field_name in (u'support_plugins', u'plugins'):
					if field_name == u'support_plugins':
						field_name = u'support-plugins'
					arglist = field_value.split((u','))
					tempvalue= u'[\n'
					for list_index in range(len(arglist)):
						tempvalue += u'    "plugin/{}"'.format(arglist[list_index])
						if not list_index == len(arglist) - 1:
							tempvalue +=u',\n'
						elif list_index == len(arglist) -1:
							tempvalue +=u'\n  ]'

					Logger.Debug(__name__, GT(u'输出 {} 变量').format(field_name))
					info_list.append(u'"{}": {}'.format(field_name, tempvalue))

		return u'{\n  ' + u',\n  '.join(info_list) + u'\n}\n'


	## Saving project
	def GetSaveData(self):
		data = self.GetInfo()
		return u'<<INFO>>\n{}<</INFO>>'.format(data)


	## Reads & parses page data from a formatted text file
	#
	#  TODO: Use 'Set'/'SetPage' method
	#
	#  \param filename
	#	File path to open
	def ImportFromFile(self, filename):
		Logger.Debug(__name__, GT(u'Importing file: {}'.format(filename)))

		if not os.path.isfile(filename):
			ShowErrorDialog(GT(u'文件: {}不存在'.format(filename)), linewrap=600)
			return dbrerrno.ENOENT

		file_text = ReadFile(filename)

		# Reset fields to default before opening
		self.Reset()
		
		self.Set(file_text)


	## Displays a file open dialog for selecting a text file to read
	def OnBrowse(self, event=None):
		browse_dialog = GetFileOpenDialog(GetMainWindow(), GT(u'打开文件'))
		if ShowDialog(browse_dialog):
			self.ImportFromFile(browse_dialog.GetPath())


	## Creates a formatted preview of the control file text
	def OnPreviewInfo(self, event=None):
		info = self.GetInfo()

		preview = TextPreview(title=GT(u'Info文件预览'),
				text=info, size=(600,400))

		ShowDialog(preview)


	## Opens a file save dialog to export info file data
	def OnSave(self, event=None):
		# Get data to write to info file
		info = self.GetInfo()

		save_dialog = GetFileSaveDialog(GetMainWindow(), GT(u'保存info信息'))
		save_dialog.SetFilename(u'info')

		if ShowDialog(save_dialog):
			# Be sure not to strip trailing newline (dpkg is picky)
			WriteFile(save_dialog.GetPath(), info, noStrip=u'\n')


	## TODO: Doxygen
	#
	#  FIXME: Unfinished???
	def ReLayout(self):
		# Organize all widgets correctly
		lc_width = self.coauth.GetSize()[0]
		self.coauth.SetColumnWidth(0, lc_width/2)


	## Resets all fields on page to default values
	def Reset(self):
		for I in self.grp_input:
			# Calling 'Clear' on ComboBox removes all options
			if isinstance(I, (wx.ComboBox, OwnerDrawnComboBox,)):
				I.SetValue(wx.EmptyString)

			else:
				I.Clear()

		for C in self.arh_check:
			C.SetValue(C.Default)

		for C in self.per_check:
			C.SetValue(C.Default)


	## Fills page's fields with input data
	#
	#  \param data
	#	Text to be parsed for values
	#  \return
	#	Leftover text to fill out 'Dependecies' page fields
	def Set(self, data):
		# Decode to unicode string if input is byte string
		if isinstance(data, str):
			data = data.decode(u'utf-8')

		# Strip leading & trailing spaces, tabs, & newlines
		data = data.strip(u' \t\n')
		info_data = data.split(u'\n')		
		
		support_plugins = u''
		plugins = u''
		startReadPlugins = 0;
		archList = []

		for line in info_data:
			if u':' in line:
				#判断是否读到了plugins区域
				if line.strip().startswith(u'"support-plugins"'):
					startReadPlugins = 1#开始读取support-plugins
					Logger.Debug(__name__, u'读取到:{}'.format(line))		
					continue

				elif line.strip().startswith(u'"plugins"'):
					startReadPlugins = 2#开始读取plugins
					Logger.Debug(__name__, u'读取到:{}'.format(line))
					continue

				key = line.split(u':')
				value = key[1].strip().strip(u',').strip(u'"').strip(u' ')
				key = key[0].strip().strip(u'"')			

				Logger.Debug(__name__, u'发现key:{}和value:{}'.format(key, value))				
				
				if key == 'arch':
					value = value.strip('[]').split(u',')
					for item in value:
						archList.append(item.strip('"'))

					for field in self.arh_check:
						if field.GetLabel() in archList:
							field.SetValue(True)	
						else:
							field.SetValue(False)
					continue
				
				for field in self.per_check:
					if key == field.GetName():
						if value.lower() == u'true':
							field.SetValue(True)
						else:
							field.SetValue(False)
						break				
				
				#文本区域Set the rest of the input fields
				for I in self.grp_input:
					input_name = I.GetName()
					if key == input_name and not key == u'plugins':
						I.SetValue(value)				

			else:		
				
				Logger.Debug(__name__, u'读取到:{}'.format(line))

				if u'plugin' in line:
					value = line.split(u'/')[1].strip(u',').strip(u'"')
					if startReadPlugins == 1:
						if not support_plugins == u'':
							support_plugins += u','
						support_plugins += value
					else:
						if not plugins == u'':
							plugins += u','
						plugins += value		
		
		self.ti_support_plugins.SetValue(support_plugins)
		self.ti_plugins.SetValue(plugins)




	## Imports field values from the 'Control' page
	def OnImportFromInfo(self, event=None):
		fields = (
			(self.ti_appid, inputid.PACKAGE),
			#(self.ti_name, inputid.NAME),desktop文件定义，所以暂时无法导入
			(self.ti_version, inputid.VERSION),
			)

		for F, FID in fields:
			field_value = GetFieldValue(pgid.CONTROL, FID)

			if isinstance(field_value, ErrorTuple):
				err_msg1 = GT(u'尝试导入值时出错')
				err_msg2 = u'\t错误代码: {}\n\t错误信息: {}'.format(field_value.GetCode(), field_value.GetString())
				Logger.Error(__name__, u'{}:\n{}'.format(err_msg1, err_msg2))

				continue

			if not TextIsEmpty(field_value):
				F.SetValue(field_value)

		arch_value = GetFieldValue(pgid.CONTROL, inputid.ARCH)
		self.chk_checkAmd.SetValue(False)
		self.chk_checkArm.SetValue(False)
		self.chk_checkMips.SetValue(False)
		self.chk_checkSw.SetValue(False)
		if arch_value == u'amd64':
			self.chk_checkAmd.SetValue(True)
		elif arch_value == u'arm64':
			self.chk_checkArm.SetValue(True)
		elif arch_value == u'mipsel' or arch_value == u'mips':
			self.chk_checkMips.SetValue(True)
		elif arch_value == u'sw':
			self.chk_checkSw.Value(True)
		elif arch_value == u'all':
			self.chk_checkAmd.SetValue(True)
			self.chk_checkArm.SetValue(True)
			self.chk_checkMips.SetValue(True)
			self.chk_checkSw.SetValue(True)

