# -*- coding: utf-8 -*-

## \package globals.tooltips
#
#  Defines tooltips that have longer texts

# MIT licensing
# See: docs/LICENSE.txt


import wx

from dbr.language		import GT
from dbr.log			import Logger
from dbr.templates		import local_templates_path
from globals.changes	import section_delims
from globals.execute	import GetSystemInstaller
from globals.ident		import btnid
from globals.ident		import pgid
from wiz.helper			import FieldEnabled


# *** Wizard buttons ***#
TT_wiz_prev = wx.ToolTip(GT(u'上一页'))
TT_wiz_next = wx.ToolTip(GT(u'下一页'))

TT_control = {
	u'btn browse': GT(u'打开预格式化好的文本'),
	u'btn save': GT(u'保存信息到文本'),
	u'btn preview': GT(u'预览文件'),
	u'package': GT(u'包/软件名'),
	u'version': GT(u'包/软件发布版本'),
	u'maintainer': GT(u'包/软件维护者全名'),
	u'email': GT(u'包/软件维护者电子邮件地址'),
	u'architecture': (
		GT(u'软件/包运行在那个平台'), u'',
		GT(u'all = 平台不相关'),
		),
	u'section': GT(u'类别，包管理者依据分类对这个包进行分类  '),
	u'priority': GT(u'这个包升级的紧迫性'),
	u'synopsis': GT(u'一行描述文本/synopsys'),
	u'description': (
		GT(u'更多详细的描述'), u'',
		GT(u'这里可以使用多行描述文本,但是太长了lintian将会抱怨')
		),
	u'source': GT(u'上游源包名'),
	u'homepage': GT(u'上游源主页网址'),
	u'essential': GT(u'为了(保证)系统的稳定，该包是否是必须的'),
}

TT_depends = {
	u'btn browse': TT_control[u'btn browse'],
	u'btn save': TT_control[u'btn save'],
	u'btn preview': TT_control[u'btn preview'],
	u'package': GT(u'依赖/冲突的包名'),
	u'operator': GT(u'操作符'),
	u'version': GT(u'和包名以及操作符相匹配的版本'),
	u'depends': GT(u'将需要被安装的包'),
	u'pre-depends': GT(u'将需要被安装和先行设置的包'),
	u'recommends': GT(u'被强烈推荐并且将会默认安装的包'),
	u'suggests': GT(u'可能有用但是不是特别必须且将会默认安装的包'),
	u'enhances': GT(u'这个包可能会对增强包（的功能）有作用'),
	u'conflicts': GT(u'如果已经被安装将被卸载的包'),
	u'replaces': GT(u'包或者它的文件将被覆盖'),
	u'breaks': GT(u'包冲突且将会被破坏配置'),
	u'btn add': GT(u'添加依赖包到列表'),
	u'btn append': GT(u'向列表里到一个选择好的依赖包添加可选择项'),
	u'btn remove': GT(u'从列表里移除选择好的依赖包'),
	u'btn clear': GT(u'清除依赖包列表'),
	u'list': GT(u'将要被添加的依赖'),
}

TT_files = {
	u'individually': GT(u'左下侧被选中目录下的所有文件将被逐个地添加到指定目标目录中'),
	u'nofollow-symlink':GT(u'不跟踪快捷方式'),
	u'btn add': GT(u'添加选中的文件/文件夹到列表'),
	u'btn remove': GT(u'从列表中删除选中的文件'),
	u'btn clear': GT(u'清除文件列表'),
	u'btn browse': GT(u'浏览选定目标安装目录'),
	u'btn refresh': GT(u'更新文件\' 可执行状态 & 可用性\''),
	u'target': GT(u'文件的目标安装目录'),
	u'filelist': (
			GT(u'此区域为文件列表区域\n通过左侧目录树中或者从文件浏览器中拖动，\n将已准备好的文件夹或者文件添加到本区域的文件列表'),
			GT(u'文件列表中的文件将在构建时被添加到相应的目标目录'), 
			GT(u'蓝色的字体 = 目录'),
			GT(u'红色字体 = 可执行'),
			GT(u'红色的背景 = 丢失文件/文件夹'),
			)
}
TT_icons = {
	u'nofollow-symlink':GT(u'不跟踪快捷方式'),
	u'btn add': GT(u'添加选中的图标文件/文件夹到列表'),
	u'btn remove': GT(u'从列表中删除选中的图标文件'),
	u'btn clear': GT(u'清除图标文件列表'),
	u'btn browse': GT(u'浏览选定目标安装目录'),
	u'btn refresh': GT(u'更新图标文件\' 可执行状态 & 可用性\''),
	u'target': GT(u'图标文件的目标安装目录'),
	u'filelist': (
			GT(u'此区域为图标文件列表区域\n通过左侧目录树中或者从文件浏览器中拖动，\n将已准备好的图标文件添加到本区域的文件列表'),
			GT(u'一般地，图标文件的名称应该为{包名}.{后缀名}'),
			GT(u'文件列表中的文件将在构建时被添加到相应的目标目录'), 
			GT(u'蓝色的字体 = 目录'),
			GT(u'红色字体 = 可执行'),
			GT(u'红色的背景 = 丢失文件/文件夹'),
			)
}

TT_info = {
	u'btn browse':GT(u'打开编辑好的info文件'),
	u'btn save':GT(u'保存info信息到文件'),
	u'btn prev':GT(u'预览info文件'),
	u'appid':GT(u'应用唯一标识，一般为包名'),
	u'name':GT(u'应用显示的名称'),
	u'version':GT(u'格式为：{MAJOR}.{MINOR}.{PATCH}.{BUILD}，版本号均为数字'),
	u'architecture':GT(u'应用支持架构'),
	u'checkamd64':GT(u'X86架构CPU'),
	u'checkarm64':GT(u'ARM架构CPU'),
	u'checkmips64':GT(u'龙芯系列CPU'),
	u'checksw64':GT(u'申威CPU'),
	u'btn import':GT(u'从control页面导入信息'),
	u'autostart':GT(u'是否允许自启动'),
	u'notification':GT(u'是否允许使用通知'),
	u'trayicon':GT(u'是否运行显示托盘图标'),
	u'clipboard':GT(u'是否允许使用剪切板'),
	u'account':GT(u'是否允许读取登录用户信息'),
	u'bluetooth':GT(u'是否允许使用蓝牙设备'),
	u'camera':GT(u'是否允许使用视频设备'),
	u'audio_record':GT(u'是否允许进行录音'),
	u'installed_apps':GT(u'是否允许读取安装软件列表'),
	u'support_plugins':GT(u'多个项目用英文逗号隔开,如：demo1，demo2'),
	u'plugins':GT(u'多个项目用英文逗号隔开，如：webbrowser,office')
}


TT_manpages = {
	u'add': GT(u'Add manpage'),
}

TT_scripts = {
	u'preinst': GT(u'包安装前运行的脚本'),
	u'postinst': GT(u'包安装后运行的脚本'),
	u'prerm': GT(u'包卸载前运行脚本'),
	u'postrm': GT(u'包卸载后运行脚本'),
	u'script body': GT(u'Script text body'),
	u'target': GT(u'脚本创建快捷方式的目录'),
	u'al list': GT(u'文件列表中被链接的可运行文件'),
	u'btn import': GT(u'导入从文件页面标记的可运行文件'),
	u'btn remove': GT(u'从列表中移除选择的可运行文件'),
	u'btn build': GT(u'生成脚本'),
	u'btn help': GT(u'如何使用快速链接'),
}

TT_changelog = {
	u'package': TT_control[u'package'],
	u'version': TT_control[u'version'],
	u'dist': (
		GT(u'Debian/Ubuntu等目标发行版的名称'), u'',
		GT(u'可以通过 "选项 ➜ 更新发行版命名缓存" 来更新此列表.')
		),
	u'urgency': TT_control[u'priority'],
	u'maintainer': TT_control[u'maintainer'],
	u'email': TT_control[u'email'],
	u'changes': (
		GT(u'List new changes here, separated one per line'), u'',
		GT(u'The first line will be prepended with an asterix (*) automatically. To denote any other sections, put one of the following as the first character on the line:'),
		u'\t{}'.format(u',  '.join(list(section_delims))),
		),
	u'target default': GT(u'安装更新日志的标准目录'),
	u'target custom': GT(u'安装更新日志的定制目录'),
	u'btn import': GT(u'从Control页面导入信息'),
	u'btn add': GT(u'在更新之上追加一个新的日志内容（log entry）'),
	u'indent': GT(u'不要删去规律的行的前置空格'),
	u'log': GT(u'格式化的更新日志内容(可编辑的)'),
}

no_lic_templates = GT(u'没有许可证模板可用')
TT_copyright = {
	u'list_disabled': no_lic_templates,
	u'full': (
		GT(u'全部标准'), u'',
		GT(u'复制系统,应用,或者本地的一个许可证'),
		GT(u'系统:'), u'\t{}'.format(GT(u'拷贝文本来自于存储于以下位置的许可证书')),
		u'\t/usr/share/common-licenses',
		GT(u'应用:'), u'\t{}'.format(GT(u'拷贝一个使用Debreate发布的标准')),
		GT(u'本地:'), u'\t{}'.format(GT(u'拷贝一个来自于以下位置的用户定义标准')),
		u'\t{}'.format(local_templates_path),
		),
	u'full_disabled': no_lic_templates,
	u'short': (
		GT(u'简短标准'), u'',
		GT(u'创建一个版权许可头& 简短地指向一个标准系统许可证书，证书来自于以下位置/usr/share/common-licenses'),
		),
	u'short_disabled': no_lic_templates,
}

TT_menu = {
	u'open': GT(u'从文件导入启动器'),
	u'export': GT(u'导出启动器到文本文件'),
	u'preview': GT(u'预览启动器文本'),
	u'filename': GT(u'定制的文件名用来做启动器'),
	u'filename chk': GT(u'除非选中, filename的值将被用作启动器的输出文件名'),
	u'name': GT(u'启动器上显示的名称'),
	u'exec': GT(u'启动的可执行程序'),
	u'comment': GT(u'光标悬停在启动器上显示的文字'),
	u'icon': GT(u'启动器显示的图标，建议使用相对路径'),
	u'mime':GT(u'应用程序支持的关联MIME类型'),
	u'type': (
		GT(u'启动器类型'), u'',
		GT(u'Application:'), u'\t{}'.format(GT(u'应用程序的快捷方式')),
		GT(u'Link:'), u'\t{}'.format(GT(u'web URL的快捷方式')),
		GT(u'Directory:'), u'\t{}'.format(GT(u'包含元数据和菜单项的容器')),
		),
	u'terminal': GT(u'表明应用是否运行在一个终端中'),
	u'startupnotify': GT(u'启动时在系统面板显示一个通知'),
	u'encoding': GT(u'设置启动器被用来读取的编码'),
	u'category': GT(u'程序的类别决定了启动器在系统菜单项中的位置'),
	u'add category': GT(u'添加当前的分类到列表'),
	u'rm category': GT(u'移除列表中选中的分类'),
	u'clear categories': GT(u'清除分类列表'),
	u'categories': GT(u'分类决定了启动器在系统菜单项中的位置'),
	u'no disp': GT(u'This options means "This application exists, but don\'t display it in the menus"'),
	u'show in': GT(u'只有当选项满足时启动器才会显示'),	
	u'other': (
		GT(u'除了以上可使用项目外混杂项目的领域'), u'',
		GT(u'点击 "帮助 ➜ 链接 ➜ 启动器 / 桌面快捷方式" 获得更多可用选项'), u'',
		GT(u'警告:'),
		u'\t{}'.format(GT(u'格式不正确的文本可能会导致启动器不可用')),
		)
}

TT_build = {
	u'md5': GT(u'为包内所有组织好的文件创建一个校验和'),
	u'md5_disabled': GT(u'使用此选项需要安装md5sum包'),
	u'strip': (
		GT(u'从二进制文件中丢弃无用的修改'), u'',
		GT(u'See "man 1 strip"'),
		),
	u'strip_disabled': GT(u'使用此选项需要安装binutils包'),
	u'rmstage': GT(u'包创建好后删除组织好的目录树'),
	u'lintian': (
		GT(u'通过lintian的规定检查包的警告和错误'), u'',
		GT(u'了解更多点击 "帮助 ➜ 链接 ➜ Lintian具体描述"'),
		),
	u'lintian_disabled': GT(u'使用此选项需要安装lintian包'),
	btnid.BUILD: GT(u'开始构建'),
	u'install': (
		GT(u'构建完毕后使用系统安装器安装包'), u'',
		u'{} {}'.format(GT(u'系统安装器请关注:'), GetSystemInstaller()),
		),
	u'install_disabled': (
		GT(u' 使用此选项需要安装以下包中的一个:'), u'',
		GT(u'gdebi-gtk, gdebi-kde'),
		),
}


TT_pages = {
	pgid.CONTROL: TT_control,
	pgid.DEPENDS: TT_depends,
	pgid.FILES: TT_files,
	pgid.ICONS: TT_icons,
	pgid.INFO:TT_info,
	pgid.MAN: TT_manpages,
	pgid.SCRIPTS: TT_scripts,
	pgid.CHANGELOG: TT_changelog,
	pgid.COPYRIGHT: TT_copyright,
	pgid.MENU: TT_menu,
	pgid.BUILD: TT_build,
}


## Universal function for setting window/control tooltips
def SetToolTip(tooltip, control, required=False):
	if isinstance(tooltip, wx.ToolTip):
		tooltip = tooltip.GetTip()

	elif isinstance(tooltip, (tuple, list)):
		tooltip = u'\n'.join(tooltip)

	if tooltip:
		if required:
			tooltip = u'{}\n\n{}'.format(tooltip, GT(u'Required'))

		control.SetToolTipString(tooltip)


## Sets multiple tooltips at once
def SetToolTips(tooltip, control_list, required=False):
	for FIELD in control_list:
		SetToolTip(tooltip, FIELD, required)


def SetPageToolTips(parent, page_id=None):
	control_list = []

	if not page_id:
		page_id = parent.GetId()

	# Recursively set tooltips for children
	for FIELD in parent.GetChildren():
		control_list.append(FIELD)

		sub_children = FIELD.GetChildren()
		if sub_children:
			SetPageToolTips(FIELD, page_id)

	if page_id in TT_pages:
		for FIELD in control_list:
			tooltip = None

			# Use ID first
			field_id = FIELD.GetId()
			if field_id in TT_pages[page_id]:
				tooltip = TT_pages[page_id][field_id]

			else:
				try:
					name = FIELD.tt_name.lower()

				except AttributeError:
					try:
						name = FIELD.GetName().lower()

					except AttributeError:
						Logger.Warn(__name__, u'Object has no name, not setting tooltip: {}'.format(type(FIELD)))

						continue

				required = False
				if name:
					if u'*' in name[-2:]:
						required = True

					# The » character causes a different tooltip to be set for disabled fields
					if u'»' in name[-2:] and not FieldEnabled(FIELD):
						name = u'{}_disabled'.format(name)

					name = name.replace(u'*', u'')
					name = name.replace(u'»', u'')

				if name in TT_pages[page_id]:
					tooltip = TT_pages[page_id][name]

			if tooltip:
				SetToolTip(tooltip, FIELD, required)
